import Main from "../components/Main.vue"
import post from "../components/Post.vue"
import theme from "../components/theme.vue"
import hot from "../components/hot.vue"
// const login = resolve => require(["../components/view/index.vue"], resolve);
// const index = resolve => require(["../components/view/index.vue"], resolve);
// const test = resolve => require(["../components/view/test.vue"], resolve);


export default [{
  path: '/',
  component: Main,
  name: 'main'

  },
  {
    path: '/post/:id',
    component: post,
    name: 'post',
    props: true
  },
  {
    path: '/theme/:id',
    component: theme,
    name: 'theme',
    props: true
  },
  {
    path: '/hot',
    component: hot,
    name: 'hot',
    props: true
  }

]